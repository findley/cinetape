#include <EEPROM.h>
#include <Wire.h>

#include "Adafruit_LEDBackpack.h"
#include "Adafruit_GFX.h"
Adafruit_7segment matrix = Adafruit_7segment();

//Maxbotix ----
const int pwPin1 = 6;         //PWM input
const int btnPin = 7;
const int sensorVinPin = 11; //PWM output for powering sensor

long filterDistance, filterOutput;

int closeMode = false;
int closeGate = 16;
int farMode = false;
int farGate = 28;
int lastBtnDown = 0;
int lastEncChange = 0;


const int MODE_SENSE   = 0;
const int MODE_MENU    = 1;
const int MODE_MENU_SN = 100;
const int MODE_MENU_BR = 101;
const int MODE_MENU_FP = 102;
const int MODE_MENU_RE = 103;
const int MODE_MENU_CF = 104;
const int MODE_MENU_FR = 105;
const int MODE_MENU_RS = 106;

volatile byte aFlag = 0;
volatile byte bFlag = 0;
volatile byte reading = 0;

int mode = MODE_SENSE;
long setting_fp = 0;
int setting_sn = 100;
int setting_br = 15;
int setting_re = 1;
int setting_rs = 0;

int setting_fp_addr = 0;
int setting_sn_addr = 4;
int setting_br_addr = 8;
int setting_re_addr = 12;

#define ENC_1_A 0
#define ENC_1_B 1

int encoder_1_value = 0;

long oldPosition    = 0;
long newPosition;

static int8_t enc_states[] = {0,-1,1,0,1,0,0,-1,-1,0,0,1,0,1,-1,0};
const char* menuItems[] = {"SN", "BR", "FP", "RE", "CF", "FR", "RS"};
int numMenuItems = 7;
int currentMenuItem = 0;

unsigned long lastSensorPrint = 0;
long lastSensorRead = 0;
long lastDistance;


void setup () {
    Serial.begin(115200);

    //sensor
    //sensorPower();
    pinMode(pwPin1, INPUT);

    //led seg matrix
    matrix.begin(0x70);
    matrix.clear();
    outRange(); //startup screen
    delay(1000); //startup screen for sensor to warm up
    pinMode(btnPin, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(btnPin), encoderButtonPressed, FALLING);

    /* Setup encoder pins as inputs */
    pinMode(ENC_1_A, INPUT_PULLUP);
    pinMode(ENC_1_B, INPUT_PULLUP);
    //attachInterrupt(digitalPinToInterrupt(ENC_1_A), PinA, RISING);
    //attachInterrupt(digitalPinToInterrupt(ENC_1_B), PinB, RISING);

    EEPROM.get(setting_sn_addr, setting_sn);
    EEPROM.get(setting_fp_addr, setting_fp);
    EEPROM.get(setting_br_addr, setting_br);
    EEPROM.get(setting_re_addr, setting_re);

}

void PinA(){
  cli(); //stop interrupts happening before we read pin values
  reading = PIND & 0x3; // read all eight pin values then strip away all but pinA and pinB's values
  if(reading == B00000011 && aFlag) { //check that we have both pins at detent (HIGH) and that we are expecting detent on this pin's rising edge
    Serial.println("--");//encoderPos --; //decrement the encoder's position count
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (reading == B00000001) bFlag = 1; //signal that we're expecting pinB to signal the transition to detent from free rotation
  sei(); //restart interrupts
}

void PinB(){
  cli(); //stop interrupts happening before we read pin values
  reading = PIND & 0x3; //read all eight pin values then strip away all but pinA and pinB's values
  if (reading == B00000011 && bFlag) { //check that we have both pins at detent (HIGH) and that we are expecting detent on this pin's rising edge
    Serial.println("++");//encoderPos ++; //increment the encoder's position count
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (reading == B00000010) aFlag = 1; //signal that we're expecting pinA to signal the transition to detent from free rotation
  sei(); //restart interrupts
}

void encoderButtonPressed() {
    if (millis() - lastBtnDown < 250) return;
    lastBtnDown = millis();

    switch(mode) {
        case MODE_SENSE:
            Serial.println("Entering menu mode");
            mode = MODE_MENU;
            break;
        case MODE_MENU:
            Serial.println("Entering sub menu mode");
            mode = currentMenuItem + 100;
            break;
        case MODE_MENU_SN:
            Serial.println("Entering sense mode");
            EEPROM.put(setting_sn_addr, setting_sn);
            mode = MODE_SENSE;
            break;
        case MODE_MENU_BR:
            Serial.println("Entering sense mode");
            EEPROM.put(setting_br_addr, setting_br);
            mode = MODE_SENSE;
            break;
        case MODE_MENU_FP:
            Serial.println("Entering sense mode");
            EEPROM.put(setting_fp_addr, setting_fp);
            mode = MODE_SENSE;
            break;
        case MODE_MENU_RE:
            Serial.println("Entering sense mode");
            EEPROM.put(setting_re_addr, setting_re);
            mode = MODE_SENSE;
            break;
        case MODE_MENU_RS:
            Serial.println("Entering sense mode");
            if (setting_rs = 1)
            {
              for (int i = 0 ; i < EEPROM.length() ; i++) {
                EEPROM.write(i, 0); //clear eeprom
              }
            }
            else
            {
              //do nothing
            }
            mode = MODE_SENSE;
            break;    
        default:
            mode = MODE_SENSE;
    }
     
    if (mode == MODE_SENSE) {
    }
    else if (mode == MODE_MENU) {
    }
}


//void display(int number) {
//    matrix.print(number);
//    matrix.writeDisplay(); 
//}
//
//void display(int left, int right) {
//    int d1 = left / 10;
//    int d2 = left % 10;
//    int d3 = right / 10;
//    int d4 = right % 10;
//    
//    if (d1 == 0) {
//        //if feet is 0 write nothing to led display
//        matrix.writeDigitRaw(0, 0);
//        if (d2 == 0) {
//            matrix.writeDigitRaw(0, 0);
//        } else {
//            matrix.writeDigitNum(1, d2);
//        }
//    } else {
//        //else print feet digit 1
//        matrix.writeDigitNum(0, d1);
//        matrix.writeDigitNum(1, d2);
//    }
//    
//    //print digit 3 and digit four even if 0
//    matrix.writeDigitNum(3, d3);
//    matrix.writeDigitNum(4, d4);
//    matrix.writeDisplay();
//}

void display(const char* string) {
    matrix.writeDigitRaw(3, 0);
    matrix.writeDigitRaw(4, 0);
    matrix.writeDigitRaw(0, getCharCode(string[0]));
    matrix.writeDigitRaw(1, getCharCode(string[1]));
    matrix.writeDisplay();
}

long readSensor() {
    long r = pulseIn(pwPin1, HIGH);// + setting_fp;
    if (millis() - lastSensorRead > 150) {
      return r;
      lastSensorRead = millis();
    }
}

void sensorPrint(long printReading, bool indicator = false) {

    float cm = printReading / 10; // converts range to cm
    int inches = int((cm / 2.54) + 0.5); // converts range to inches
    int feet = inches / 12; // converts range to feet
    int remInches = inches % 12; // remaining inches from feet
    
    //d1-dft seperates digits to individual numbers to be printed to led display
    int d1 = feet / 10;
    int d2 = feet % 10;
    int d3 = remInches / 10;
    int d4 = remInches % 10;
    int dft = d1 + d2;

    if (d1 == 0) {
        //if feet is 0 write nothing to led display
        matrix.writeDigitRaw(0, 0);
        //matrix.writeDigitNum(2,0);
    } else {
        //else print feet digit 1
        matrix.writeDigitNum(0, d1);
        //matrix.writeDigitNum(2,1);
    }
    if (d1 >= 1) {
        //if digit 1 is greater or equal to (1) print digit 2
        matrix.writeDigitNum(1, d2);
    } else if (dft == 0) {
        //if digit 1 plus digit 2 is equal to 0 print blank
        matrix.writeDigitRaw(1, 0);
        //matrix.writeDigitNum(2,0);
    } else {
        //else print digit 2
        matrix.writeDigitNum(1, d2, true); //matrix.writeDigitNum(1, d2, true) for decimal
        //matrix.writeDigitNum(2,1);
    }
    //print digit 3 and digit four even if 0
    matrix.writeDigitNum(3, d3);
    matrix.writeDigitNum(4, d4, indicator);

    //resolution setting -- updates display if subject moves two inches instead of one
    if ((remInches % 2 == 0) && setting_re == 2)
    {
        //only updates display if unit changes two inches
        matrix.writeDisplay();
    }
    else if (setting_re == 1)
    {
        //update display in 1 inch incriments (normal operation)
        matrix.writeDisplay();
    }
    else
    {
        //don't print display unless moved 2 inches
    }

}

void sensorPower() {
    //PWM sensor power to reduce beam pattern
    setting_sn = constrain(setting_sn, 5, 100);
    int sensorOut = map(setting_sn, 5, 100, 150, 255);
    sensorOut = constrain(sensorOut, 150, 255);
    analogWrite(sensorVinPin, sensorOut); //analogWrite(pin, value) between 0 (always off) and 255 (always on) 131 = 2.5v 191 = 3.7v 255 = 5v
}


void ledBrightness() {
    //led display brightness
    setting_br = constrain(setting_br, 0, 15);
    matrix.setBrightness(setting_br); //0 to 15 LED 7 seg brightness adjust

}

void outRange() {
    //print out of range
    matrix.writeDigitRaw(0, 99); // 99 = "high o"
    matrix.writeDigitRaw(1, 92); // 92 = "low o"
    matrix.writeDigitRaw(3, 99); // 92 = "high o"
    matrix.writeDigitRaw(4, 92); // 99 = "low o"
    matrix.writeDisplay();
}

/* returns change in encoder state (-1,0,1) */
int8_t read_encoder_raw() {
    static uint8_t old_AB = 0;
    //static int8_t encval = 0;     //encoder value
    
    old_AB <<= 2;
    old_AB |= ((digitalRead(ENC_1_B))?(1<<1):0) | ((digitalRead(ENC_1_A))?(1<<0):0);
    return ( enc_states[( old_AB & 0x0f )]);
}

/**
 * Returns:
 * -1: When encoder moves backward
 *  0: When encoder has not moved
 *  1: When encoder moves forward
 */
int getEncoderChange() {
    int8_t encoderdata1 = read_encoder_raw();
    if ( encoderdata1 ) {
        encoder_1_value += encoderdata1;
        newPosition = encoder_1_value/4;
    }
    if (newPosition > oldPosition ) {
        oldPosition = newPosition;
        return 1;
    }
    else if (newPosition < oldPosition) {
        oldPosition = newPosition;
        return -1;
    }

    return 0;
}

void loop () {
    int encoderChange;
    switch(mode) {
        case MODE_SENSE:
            sensorPrint(readSensor() + setting_fp); //prints sensor data to LED display
            break;
        case MODE_MENU:
            display(menuItems[currentMenuItem]);
            encoderChange = getEncoderChange();
            if (encoderChange > 0) {
                currentMenuItem = (currentMenuItem + 1) % numMenuItems;
                Serial.print("Displaying: ");
                Serial.println(menuItems[currentMenuItem]);
            } else if (encoderChange < 0) {
                currentMenuItem -= 1;
                if (currentMenuItem < 0) currentMenuItem = numMenuItems - 1;
                Serial.print("Displaying: ");
                Serial.println(menuItems[currentMenuItem]);
            }
            break;
        case MODE_MENU_SN:
            sensorPower();
            matrix.print(setting_sn);
            matrix.writeDisplay();
            encoderChange = getEncoderChange();
            if (encoderChange > 0) {
                setting_sn += 5;
                Serial.println("Sn UP");
            } else if (encoderChange < 0) {
                setting_sn -= 5;
                Serial.println("Sn DOWN");
            }
            break;
        case MODE_MENU_BR:
            ledBrightness();
            matrix.print(setting_br);
            matrix.writeDisplay();
            encoderChange = getEncoderChange();
            if (encoderChange > 0) {
                setting_br += 1;
                Serial.println("Br UP");
            } else if (encoderChange < 0) {
                setting_br -= 1;
                Serial.println("Br DOWN");
            }
            break;
        case MODE_MENU_FP:
            if (millis() - lastSensorPrint >= 150) {
                lastDistance = readSensor(); //prints sensor data to LED display
                lastSensorPrint = millis();
            }
            sensorPrint(lastDistance + setting_fp, true);
            encoderChange = getEncoderChange();
            if (encoderChange > 0) {
                setting_fp += 26;
                Serial.println("Calib UP");
            } else if (encoderChange < 0) {
                setting_fp -= 26;
                Serial.println("Calib DOWN");
            }
            break;
        case MODE_MENU_RE:
            setting_re = constrain(setting_re, 1, 2);
            matrix.print(setting_re);
            matrix.writeDisplay();
            encoderChange = getEncoderChange();
            if (encoderChange > 0) {
                setting_re += 1;
                Serial.println("Res UP");
            } else if (encoderChange < 0) {
                setting_re -= 1;
                Serial.println("Res DOWN");
            }
            break;
        case MODE_MENU_RS:
            setting_rs = constrain(setting_rs, 0, 1);
            matrix.print(setting_rs);
            matrix.writeDisplay();
            encoderChange = getEncoderChange();
            if (encoderChange > 0) {
                setting_rs += 1;
                Serial.println("Reset UP");
            } else if (encoderChange < 0) {
                setting_rs -= 1;
                Serial.println("Reset DOWN");
            }
            break;
    }
}













//------------------- menu write functions

void hornCheck() {

    //to-do function writes 'horn' to led display if sensor isn't connected
    matrix.writeDigitRaw(0, 116); // 116 = "h"
    matrix.writeDigitRaw(1, 92); // 92 = "o"
    matrix.writeDigitRaw(3, 80); // 80 = "r"
    matrix.writeDigitRaw(4, 84); // 84 = "n"
    //    matrix.writeDisplay();
    //    delay(200);

}

int getCharCode(char c) {
    switch(c) {
        case 'A':
            return 119;
        case 'B':
            return 124;
        case 'S':
            return 109;
        case 'N':
            return 84;
        case 'R':
            return 80;
        case 'F':
            return 113;
        case 'P':
            return 115;
        case 'U':
            return 62;
        case 'E':
            return 121;
        case 'C':
            return 57;
        default:
            return 16;
        break;
    } 
}
